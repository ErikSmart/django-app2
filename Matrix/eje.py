import tkinter as tk
from time import gmtime, strftime
import random
from tkinter import ttk


class App:
    def __init__(self):
        self.valor = "Hola mundo"
        self.ventana1 = tk.Tk()
        self.ventana1.title("La venta")

        self.cuaderno1 = ttk.Notebook(self.ventana1)
        self.pagina1 = ttk.Frame(self.cuaderno1)
        self.cuaderno1.add(self.pagina1, text="Button")

        self.label2 = ttk.Button(self.cuaderno1, text="Es nadal")
        self.label2.grid(column=0, row=1)

        self.cuaderno1.grid(column=1, row=0)

        self.dato = tk.StringVar()
        self.dato2 = tk.StringVar()
        self.nombre = tk.StringVar()
        self.label = tk.Label(self.ventana1, text=self.valor)
        self.label.grid(column=0, row=0)
        self.label.configure(foreground="red", width=15, height=15)

        # Boton
        self.boton = tk.Button(
            self.ventana1, text="Click aqui", command=self.saludo)
        self.boton.grid(column=0, row=3)
        self.boton.configure(width=10, height=2)

        self.boton2 = tk.Button(text="Cambiar mensaje", command=self.des)
        self.boton2.grid(column=0, row=1)
        self.boton2.configure(width=10, height=2)

        self.lab_nombre = tk.Label(text="Nombre", width=10)
        self.lab_nombre.grid(column=0, row=7)
        self.nombre = tk.Entry(self.ventana1, width=10,
                               textvariable=self.nombre)
        self.nombre.grid(column=0, row=8)

        self.entrada = tk.Entry(self.ventana1, width=10,


                                textvariable=self.dato)
        self.entrada2 = tk.Entry(
            self.ventana1, width=10, textvariable=self.dato2)
        self.entrada.grid(column=0, row=5)
        self.entrada2.grid(column=0, row=6)

        self.ventana1.mainloop()

    def saludo(self):
        self.valor = "Hola mundo" + self.nombre.get()
        self.label.config(text=self.valor)

    def des(self):
        self.valor = float(self.dato.get()) + " " + float(self.dato2.get())
        self.label.config(text=self.valor)


r = App()
