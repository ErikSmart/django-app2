from django.db import models

# Create your models here.
# No olvidar modificar admin.py para importar el modelo para que se puedan ser visibles las tablas creadas eje. admin.site.register(Servicio, ServicioAdmin)


class Servicio(models.Model):
    title = models.CharField(verbose_name="Titulo", max_length=30)

    subtitle = models.CharField(verbose_name="Subtitulo", max_length=50)
    contenido = models.TextField(verbose_name="Contenido")
    imagen = models.ImageField(verbose_name="Imagenes")
    created = models.DateField(verbose_name="Creado", auto_now_add=True)
    updated = models.DateField(verbose_name="Actulizado", auto_now=True)

    def __str__(self):
        return self.title


class meta:
    verbose_name = "Servicio"
    verbose_name_plural = "Servicios"
