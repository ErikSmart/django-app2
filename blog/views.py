from django.shortcuts import render, get_object_or_404
from .models import Post, Categoria
from django.views.generic.base import TemplateView

# Create your views here.

# Usando Template View con clase en lugar de solo fucniones
# def blog(request):
#    post = Post.objects.all()
#    return render(request, "blog/blog.html", {'post': post})


class BlogView(TemplateView):
    template_name = "blog/blog.html"
    # Sobrescribiendo el metodo get

    def get(self, request, *args, **kwargs):
        post = Post.objects.all()
        return render(request, self.template_name, {'post': post})


def categoria(request, categoria_id):
    categoria = get_object_or_404(Categoria, id=categoria_id)
    return render(request, "blog/categoria.html", {'categoria': categoria})
