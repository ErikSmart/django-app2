from django.contrib import admin
from .models import Categoria, Post


class CategoriaAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')
  # sobre escribe el metodo para los permisos de los campos ejemplo si en el if pongo 'nombre' desaparecera el campo textfile

    def get_readonly_fields(self, request, obj=None):
        if request.user.groups.filter(name='Persona').exists():
            return ('updated', 'created')
        else:
            return ('updated')


class PostAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')
    list_display = ('titulo', 'autor',)
    search_fields = ('titulo',)


# Register your models here.
admin.site.register(Categoria, CategoriaAdmin)
admin.site.register(Post, PostAdmin)
