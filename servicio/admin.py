from django.contrib import admin
from .models import Servicio

# Register your models here.

# Clase para creada para que los campos sean solo de lectura


class ServicioAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')


# Es importate para que aparescan las tablas en el
admin.site.register(Servicio, ServicioAdmin)
