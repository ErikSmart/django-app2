# importar redirect para poder redireccionar
from django.shortcuts import render, redirect
# Se declara creando un archivo forms.py
from .forms import ContactoFormulario
# redireccionar antes importar redirect
from django.urls import reverse
# Libreria de correo
from django.core.mail import EmailMessage

# Create your views here.


def contacto(request):
    # Instanciando el formulario
    contacto = ContactoFormulario()
    # Mostrar el tipo de metodo
    metodo = "Esto {} es ".format(request.method)
    form = ContactoFormulario()
    if request.method == "POST":
        form = ContactoFormulario(data=request.POST)
        if form.is_valid():
            # Ocultando mensaje
            nombre = request.POST.get('nombre', '')
            correo = request.POST.get('correo', '')
            mensaje = request.POST.get('mensaje', '')
            correo = EmailMessage(
                "Matrix Web",
                "De {} <{}>".format(nombre, correo, mensaje),
                "nocontestar@mail.com",
                ["ek009@msn.com"],
                reply_to=[correo]
            )
            try:
                correo.send()
                # redireccionado con un parametro url al final y enviamos
                return redirect(reverse('contacto')+"?ok")
            except:
                # redireccionado con un parametro url al final y enviamos
                return redirect(reverse('contacto')+"?fallo")

    return render(request, "contacto/contacto.html", {'form': contacto, 'metodo': metodo, 'form': form})
