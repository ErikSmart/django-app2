from django import forms

# Agregando los estilos css a los campos con attrs


class ContactoFormulario(forms.Form):
    nombre = forms.CharField(label="Nombre", required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Escribe tu nombre'}
    ))
    correo = forms.CharField(label="Correo", required=True, widget=forms.EmailInput(
        attrs={'class': 'form-control', 'placeholder': 'Escibe tu correo'}
    ))
    mensaje = forms.CharField(
        label="Contenido", required=True, widget=forms.Textarea(
            attrs={'class': 'form-control', 'rows': 6,
                   'placeholder': 'El mensaje'}
        ))
