from django.shortcuts import render, HttpResponse, get_object_or_404
from servicio.models import Servicio


# Create your views here.


def inicio(request):
    return render(request, "core/inicio.html")


def historia(request):
    return render(request, "core/historia.html")


def servicio(request):
    serviciox = Servicio.objects.all()

    return render(request, "core/servicio.html", {'servicio': serviciox})


def store(request):
    return render(request, "core/store.html")


def base(request):
    return render(request, "core/base.html")
