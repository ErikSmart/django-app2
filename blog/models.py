from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from ckeditor.fields import RichTextField

# Create your models here.


class Categoria(models.Model):
    nombre = models.CharField(verbose_name="Nombre categora", max_length=30)
    created = models.DateTimeField(verbose_name="Creado", auto_now_add=True)
    updated = models.DateTimeField(verbose_name="Actulizado", auto_now=True)
    archivo = models.DateTimeField(verbose_name="algo", auto_now=True)

    class Meta:
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorias'
        ordering = ['-created']

    def __str__(self):
        return self.nombre


class Post(models.Model):
    titulo = models.CharField(verbose_name="Titulo", max_length=30)
    contenido = RichTextField(verbose_name="Contenido")
    publicado = models.DateTimeField(
        verbose_name="Publicado", default=timezone.now())
    imagen = models.ImageField(
        verbose_name="Image", upload_to='blog', null=True, blank=True)
    autor = models.ForeignKey(
        User, verbose_name="El Autor", on_delete=models.CASCADE)
    # Relacion de tablas Categorias post con el campor personalizado con related_name="categorias_post" revisar blog.html
    categorias = models.ManyToManyField(
        Categoria, verbose_name="Categorias", related_name="categorias_post")
    created = models.DateTimeField(verbose_name="Creado", auto_now_add=True)
    updated = models.DateTimeField(verbose_name="Actulizado", auto_now=True)

    class Meta:
        verbose_name = 'Entrada'
        verbose_name_plural = 'Entradas'
        ordering = ['-created']

    def __str__(self):
        return self.titulo
