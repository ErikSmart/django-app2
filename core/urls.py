from django.urls import path
from . import views

urlpatterns = [
    path('', views.inicio, name="inicio"),
    path('historia', views.historia, name="historia"),
    path('store', views.store, name="store"),
    path('servicio', views.servicio, name="servicio"),
    path('base', views.base, name="base"),
]
